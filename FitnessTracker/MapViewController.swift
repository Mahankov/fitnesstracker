//
//  MapViewController.swift
//  FitnessTracker
//
//  Created by Anton Makhankov on 14.06.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var trackPosition: UIBarButtonItem!
    @IBOutlet weak var marks: UIBarButtonItem!
    
    var tracking: Bool = false
    var marking: Bool = false
    let coordinate = CLLocationCoordinate2D(latitude: 59.939095, longitude: 30.315868)
    var locationManager: CLLocationManager?
    //var manualMarker: GMSMarker?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocationManager()
    }
    
    func configureLocationManager() {
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.delegate = self
        locationManager?.requestLocation()
    }

    @IBAction func updateLocation(_ sender: Any) {
        if tracking {
            locationManager?.stopUpdatingLocation()
            tracking = false
            trackPosition.title = "Start tracking"
        } else {
            locationManager?.startUpdatingLocation()
            tracking = true
            trackPosition.title = "Stop tracking"
        }
        locationManager?.requestLocation()
    }
    
    @IBAction func currentLocation(_ sender: Any) {
        if marking {
            mapView.clear()
            marking = false
            marks.title = "Add mark"
        } else {
            marking = true
            marks.title = "Remove marks"
        }
    }
        
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let coordinates = locations.first!.coordinate
        let camera = GMSCameraPosition.camera(withTarget: coordinates, zoom: 17)
        if marking {
            let marker = GMSMarker(position: coordinates)
            marker.map = mapView
        }
        mapView.camera = camera
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
